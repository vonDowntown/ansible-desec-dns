[![pipeline status](https://gitlab.com/XenGi/ansible-desec-dns/badges/master/pipeline.svg)](https://gitlab.com/XenGi/ansible-desec-dns/commits/master)

# ansible-desec-dns

Ansible modules for deSEC DNS API using the [desec_dns_api library][pylib].

Show documentation with:

```shell script
ansible-doc -M library desec_dns_account
ansible-doc -M library desec_dns_domain
ansible-doc -M library desec_dns_rrset
ansible-doc -M library desec_dns_token
```

## desec_dns_account module

Ansible module to get information about a desec account.

Example playbook:

```yaml
---
- name: get account info
  desec_dns_account:
    token: "mysupersecuretoken123!"
  register: account_info

- name: check domain limit
  debug:
    var: account_info.account_domain_limit
```

## desec_dns_token module

_coming soon_

## desec_dns_domain module

_coming soon_

## desec_dns_rrset module

_coming soon_


---

Made with :heart: and :snake:.


[pylib]: https://gitlab.com/xengi/desec-dns-api
