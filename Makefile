.PHONY: lint pylint check check-account check-token check-domain check-rrset test test-account

lint:
	ansible-lint -x 502 **.yml

pylint:
	flake8 --max-line-length=120 --ignore=E402 library/*.py

check: check-account #check-token check-domain check-rrset

check-account:
	ansible-playbook --syntax-check ./test-account.yml

check-token:
	ansible-playbook --syntax-check ./test-token.yml

check-domain:
	ansible-playbook --syntax-check ./test-domain.yml

check-rrset:
	ansible-playbook --syntax-check ./test-rrset.yml

test: test-account

test-account:
	ansible-playbook test-account.yml
