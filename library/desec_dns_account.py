#!/usr/bin/env python3

# Copyright: (c) 2020, Ricardo Band <email@ricardo.band>
# MIT license

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'supported_by': 'community',
    'status': ['preview']
    # 'status': ['stableinterface']
}

DOCUMENTATION = '''
module: desec_dns_account

short_description: Module for deSEC DNS API account management

description:
  - "Module for the desec.io DNS service account management. Can be used to retrieve account info or delete an account."

version_added: "2.9"

author: Ricardo Band (@XenGi)

options:
  token:
    description:
      - Token that should be used to access the API
    type: str
    required: true
  password:
    description:
      - Account password. Required when I(state=absent).
    required: true
    type: str
  base_url:
    description:
      - Change the base URL to use the module with other instances of the deSEC API than the official one
    type: str
    default: "https://desec.io/api"
  state:
    description:
      - Can be used to have the account deleted
    type: str
    default: present
    choices:
      - present
      - absent

requirements:
  - "desec_dns_api~=1.0"

seealso:
  - name: desec_dns_domain
    description: Module to manage domains via deSEC DNS API
    link: https://gitlab.com/XenGi/ansible-desec-dns
  - name: desec_dns_rrset
    description: Module to manage recordsets via deSEC DNS API
    link: https://gitlab.com/XenGi/ansible-desec-dns
  - name: desec_dns_token
    description: Module to manage tokens used by deSEC DNS API
    link: https://gitlab.com/XenGi/ansible-desec-dns

notes:
  - "This module supports check_mode"
'''

EXAMPLES = '''
# Check domain limit of account
- name: Get account info
  desec_dns_account:
    token: 'mysupersecuretoken123!'
  register: account_info

- debug:
    var: account_info.account_limit_domains

# Delete account
- name: Remove account
  desec_dns_account:
    password: 'mysupersecurepassword123!'
    state: absent
'''

RETURN = '''
account_created:
    description: Account creation date
    type: str
    returned: on success
account_email:
    description: Email address of the account
    type: str
    returned: on success
account_id:
    description: UUID identifying the account
    type: str
    returned: on success
account_limit_domains:
    description: Domain limit of the account
    type: int
    returned: on success
'''

import traceback
from datetime import datetime

from ansible.module_utils.basic import AnsibleModule, missing_required_lib

LIB_IMP_ERR = None
try:
    from desec_dns_api import UnauthorizedError, UnexpectedError, NonEmptyAccountError
    from desec_dns_api.account import info, delete_account, Account
    HAS_LIB = True
except ImportError:
    HAS_LIB = False
    LIB_IMP_ERR = traceback.format_exc()


def run_module():
    module_args = dict(
        token=dict(type='str', required=True, no_log=True),
        password=dict(type='str', required=False, no_log=True),
        base_url=dict(type='str', required=False, default='https://desec.io/api'),
        state=dict(type='str', required=False, default='present')
    )

    result = dict(
        changed=False,
        account_created='',
        account_email='',
        account_id=0,
        account_limit_domains=''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # fail if desec_dns_api is not found
    if not HAS_LIB:
        module.fail_json(msg=missing_required_lib("desec_dns_api"), exception=LIB_IMP_ERR)

    if module.check_mode:
        module.exit_json(**result)

    # TODO: find a better way to test the module
    TEST_MODE = module.params['token'] == 'ansible_test_token'

    try:
        # get account info
        if TEST_MODE:
            account_info = Account(id='9ab16e5c-dead-beef-2342-af3f5a541d47',
                                   created=datetime.strptime('1970-01-01 00:00:00UTC', '%Y-%m-%d %H:%M:%S%Z'),
                                   email='youremailaddress@example.com',
                                   limit_domains=5)
        else:
            account_info = info(module.params['token'])

        result['account_created'] = account_info.created
        result['account_email'] = account_info.email
        result['account_id'] = account_info.id
        result['account_limit_domains'] = account_info.limit_domains

        # delete account
        if module.params['state'] == 'absent':
            if 'password' not in module.params.keys():
                module.fail_json(msg='You need to provide the account password to delete an account')

            if not TEST_MODE:
                delete_account(account_info.email, module.params['password'])
            module.exit_json(msg='Account deletion was requested. To finish the deletion, click on the link in the '
                                 'email that was sent to you.', changed=True)

        module.exit_json(**result)
    except NonEmptyAccountError as e:
        module.fail_json(msg=str(e))
    except UnauthorizedError as e:
        module.fail_json(msg=str(e))
    except UnexpectedError as e:
        module.fail_json(msg=str(e))


if __name__ == '__main__':
    run_module()
